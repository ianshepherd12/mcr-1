

for file in ../orig_files/*.xyz
do

  echo ${file}
  name=${file##../orig_files/}
  echo ${name}

  awk '{if (NF==4) printf("%4s%15f%12f%12f\n",$1NR-2,$2,$3,$4)}' ${file} > ${name}


done
