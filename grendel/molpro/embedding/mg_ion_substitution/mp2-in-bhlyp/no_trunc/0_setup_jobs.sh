#!/bin/bash

cp -r ../../../mp2-in-bhlyp/no_trunc/region_4_* .


for i in ./region*
do

   echo $i
   cd $i

   rm 0_setup_script.sh header footer

   for i in ./?s/*.com
   do
       #echo $i
       sed -i 's/Zn41/Mg41/' $i
   done

   cd ..

done
