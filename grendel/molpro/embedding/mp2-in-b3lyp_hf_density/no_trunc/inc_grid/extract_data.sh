#!/bin/bash

in_file=energy.dat


#list="region_1/ region_1_H466_D465/ region_1_Zn_H466_D465/ region_2/ region_2_Zn_H466_D465/ region_3/ region_3_Zn_H466_D465/ region_4/ region_4_D465/ region_4_H466/ region_4_H466_D465/ region_4_Zn/ region_4_Zn_D465/ region_4_Zn_H466/ region_4_Zn_H466_D465/ region_4_Zn_H466_D465_H478/"

echo "SCS-MP2-in-B3LYP-HF"
#for i in ${list}
for i in region*/
do 

#  echo $i

  var=$(grep "$i" ${in_file} | awk '{print $3}' | tr '\n' ' ')
  #echo $i 
  echo $i $var
  #echo ""

done



echo "HF-in-B3LYP-HF"
for i in region*/
do 

  var=$(grep "$i" ${in_file} | awk '{print $4}' | tr '\n' ' ')
  echo $i $var

done



echo "B3LYP-HF"
for i in region*/
do 

  var=$(grep "$i" ${in_file} | awk '{print $5}' | tr '\n' ' ')
  echo $i $var

done



echo "HF"
for i in region*/
do 

  var=$(grep "$i" ${in_file} | awk '{print $6}' | tr '\n' ' ')
  echo $i $var

done



echo "no elec"
for i in ${list}
do 

  var=$(grep "$i" ${in_file} | awk '{print $2}' | tr '\n' ' ')
  echo $i $var

done

