#!/bin/bash


hf_dir=../../../mp2-in-hf/no_trunc

#rm -r region_*

cp ../../../mp2-in-bhlyp/no_trunc/inc_grid/header .


for i in $hf_dir/reg*
do

region=${i#$hf_dir/}
echo $i $region

mkdir $region
cp $i/* $region/
cd $region

rm -r ./?s/
rm header
cp ../header .

sed -i 's/mp2-in-hf/mp2-in-b3lyp_hf_dens/' ./0*

#sed -i 's@header@../header@' ./0*

tail -1 footer >> embed_line
rm footer
cat ../incomplete_footer embed_line >> footer

cd ..


done



