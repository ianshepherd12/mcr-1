#!/bin/bash

job_file=*.com
job_base_name=mp2-in-b3lyp

walltime="48:0:0"
node_no=1
proc_per_node=8


rm ./?s/qscript

for i in ./?s
do

   echo ${i#./}
   job_name=${job_base_name}_${i#./}
   cd $i
   ln -s ../qscript qscript
#   ./qscript ${job_file} -N ${job_name} -l walltime=${walltime},nodes=${node_no}:ppn=${proc_per_node}:mem4G
   ./qscript ${job_file} -N ${job_name} -l walltime=${walltime},nodes=${node_no}:ppn=${proc_per_node}

   cd ../

done

