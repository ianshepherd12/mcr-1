#!/bin/bash


list=(region_4_H466 region_4_H466_D465 region_4_D465 region_4_Zn_D465 region_4_Zn_H466)


for i in ${list[@]}
do

   echo $i
   cd $i

   ./0*
   ./1*

   cd ..

done
