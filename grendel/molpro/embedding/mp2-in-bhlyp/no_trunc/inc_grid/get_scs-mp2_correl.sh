#!/bin/bash

input_file="*.out"
out_file=scs-mp2_correl_energy.dat
temp_file=data_temp.dat

rc_list=(rs ts ps)



get_scs_mp2_correl () {
grep 'SCS-MP2 correlation energy' $1 | awk '{print $4}'
}



#redirect standard error because the above function writes out to stderr if input file not present 
#could alternatively add a if [-f file]; then check to each function instead - would need to set function output to null too
exec 3>&2
exec 2> /dev/null

rm ${out_file}
rm -r ${out_folder}
mkdir ${out_folder}

rm ${elec_file}
rm ${dft_embed_energy_file}

echo "location SCS-MP2_correlation_energy" > ${temp_file}

for i in region_*
#for i in ${region_list[@]}
do

  region=$i
  echo $region

  for rc in ${rc_list[@]}
  do
    file=${region}/${rc}/${input_file}
#    echo $file
    scs_correl=$(get_scs_mp2_correl ${file})
    echo ${file} ${scs_correl} >> ${temp_file}

  done

done

#Reinstate stderr
exec 2>&3


column -t ${temp_file} > ${out_file}
rm ${temp_file}
