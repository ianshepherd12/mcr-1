#!/bin/bash

bhlyp_dir=../../mp2-in-bhlyp/no_trunc



for i in $bhlyp_dir/reg*
do

region=${i#$bhlyp_dir/}
echo $i $region

cp -r $i $region
cd $region
rm -r ./?s/
sed -i 's/mp2-in-bhlyp/mp2-in-hf/' ./0*
sed -i 's/{ks,bh-lyp}/{hf}/' footer
cd ..

done
