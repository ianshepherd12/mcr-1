g-0-0
     state = job-exclusive
     np = 8
     properties = group0,mem4G,tmp200G,np8
     ntype = cluster
     jobs = 0/1080752.grendel.chm.bris.ac.uk, 1/1080752.grendel.chm.bris.ac.uk, 2/1080753.grendel.chm.bris.ac.uk, 3/1080753.grendel.chm.bris.ac.uk, 4/1082344.grendel.chm.bris.ac.uk, 5/1082344.grendel.chm.bris.ac.uk, 6/1082350.grendel.chm.bris.ac.uk, 7/1082350.grendel.chm.bris.ac.uk
     status = rectime=1560866259,varattr=,jobs=1080752.grendel.chm.bris.ac.uk 1080753.grendel.chm.bris.ac.uk 1082344.grendel.chm.bris.ac.uk 1082350.grendel.chm.bris.ac.uk,state=free,netload=1614014595990,gres=,loadave=7.01,ncpus=8,physmem=33009308kb,availmem=63370324kb,totmem=66014868kb,idletime=17449389,nusers=2,nsessions=5,sessions=443 913 20664 27981 28029,uname=Linux g-0-0.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-0-1
     state = job-exclusive
     np = 8
     properties = group0,mem4G,tmp200G,np8
     ntype = cluster
     jobs = 0/1082319.grendel.chm.bris.ac.uk, 1/1082319.grendel.chm.bris.ac.uk, 2/1082335.grendel.chm.bris.ac.uk, 3/1082335.grendel.chm.bris.ac.uk, 4/1082335.grendel.chm.bris.ac.uk, 5/1082335.grendel.chm.bris.ac.uk, 6/1082335.grendel.chm.bris.ac.uk, 7/1082335.grendel.chm.bris.ac.uk
     status = rectime=1560866236,varattr=,jobs=1082319.grendel.chm.bris.ac.uk 1082335.grendel.chm.bris.ac.uk,state=free,netload=10113681908914,gres=,loadave=8.08,ncpus=8,physmem=33009304kb,availmem=64982300kb,totmem=66014864kb,idletime=29368426,nusers=3,nsessions=3,sessions=5043 16237 18558,uname=Linux g-0-1.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-0-9
     state = free
     np = 8
     properties = group0,np8,qchem
     ntype = cluster
     status = rectime=1560866255,varattr=,jobs=,state=free,netload=84055375817,gres=,loadave=0.00,ncpus=8,physmem=16464952kb,availmem=32565460kb,totmem=32926768kb,idletime=3970837,nusers=0,nsessions=0,uname=Linux g-0-9.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-1
     state = job-exclusive
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1080763.grendel.chm.bris.ac.uk, 1/1080472.grendel.chm.bris.ac.uk, 2/1080466.grendel.chm.bris.ac.uk, 3/1080634.grendel.chm.bris.ac.uk
     status = rectime=1560866259,varattr=,jobs=1080634.grendel.chm.bris.ac.uk 1080466.grendel.chm.bris.ac.uk 1080472.grendel.chm.bris.ac.uk 1080763.grendel.chm.bris.ac.uk,state=free,netload=10675392828361,gres=,loadave=3.30,ncpus=4,physmem=7387308kb,availmem=13314912kb,totmem=14774436kb,idletime=3035390,nusers=3,nsessions=6,sessions=22470 18829 15899 20293 25389 27744,uname=Linux g-1-1.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-2
     state = job-exclusive
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1080206.grendel.chm.bris.ac.uk, 1/1080206.grendel.chm.bris.ac.uk, 2/1080206.grendel.chm.bris.ac.uk, 3/1080206.grendel.chm.bris.ac.uk
     status = rectime=1560866272,varattr=,jobs=1080206.grendel.chm.bris.ac.uk,state=free,netload=6749020013220,gres=,loadave=1.00,ncpus=4,physmem=7387308kb,availmem=13258772kb,totmem=14774436kb,idletime=35612637,nusers=2,nsessions=2,sessions=2080 2683,uname=Linux g-1-2.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-3
     state = job-exclusive
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1080276.grendel.chm.bris.ac.uk, 1/1080471.grendel.chm.bris.ac.uk, 2/1080057.grendel.chm.bris.ac.uk, 3/1080470.grendel.chm.bris.ac.uk
     status = rectime=1560866270,varattr=,jobs=1080057.grendel.chm.bris.ac.uk 1080276.grendel.chm.bris.ac.uk 1080470.grendel.chm.bris.ac.uk 1080471.grendel.chm.bris.ac.uk,state=free,netload=7037240561283,gres=,loadave=3.02,ncpus=4,physmem=7387308kb,availmem=13064512kb,totmem=14774436kb,idletime=61529325,nusers=3,nsessions=6,sessions=16613 22347 32254 31437 30641 26558,uname=Linux g-1-3.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-4
     state = job-exclusive
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1082347.grendel.chm.bris.ac.uk, 1/1082347.grendel.chm.bris.ac.uk, 2/1082347.grendel.chm.bris.ac.uk, 3/1082347.grendel.chm.bris.ac.uk
     status = rectime=1560866268,varattr=,jobs=1082347.grendel.chm.bris.ac.uk,state=free,netload=7106665436388,gres=,loadave=3.99,ncpus=4,physmem=7387308kb,availmem=14123324kb,totmem=14774436kb,idletime=1545949,nusers=2,nsessions=2,sessions=21199 26288,uname=Linux g-1-4.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-5
     state = job-exclusive
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1080747.grendel.chm.bris.ac.uk, 1/1080747.grendel.chm.bris.ac.uk, 2/1080747.grendel.chm.bris.ac.uk, 3/1080747.grendel.chm.bris.ac.uk
     status = rectime=1560866279,varattr=,jobs=1080747.grendel.chm.bris.ac.uk,state=free,netload=7861002255732,gres=,loadave=1.00,ncpus=4,physmem=7387308kb,availmem=13305476kb,totmem=14774436kb,idletime=61529339,nusers=1,nsessions=1,sessions=23684,uname=Linux g-1-5.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-6
     state = job-exclusive
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1080747.grendel.chm.bris.ac.uk, 1/1080747.grendel.chm.bris.ac.uk, 2/1080747.grendel.chm.bris.ac.uk, 3/1080747.grendel.chm.bris.ac.uk
     status = rectime=1560866256,varattr=,jobs=1080747.grendel.chm.bris.ac.uk,state=free,netload=15855384607101,gres=,loadave=0.00,ncpus=4,physmem=7387308kb,availmem=14570028kb,totmem=14774436kb,idletime=61529322,nusers=0,nsessions=0,uname=Linux g-1-6.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-7
     state = job-exclusive
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1082394.grendel.chm.bris.ac.uk, 1/1082394.grendel.chm.bris.ac.uk, 2/1082394.grendel.chm.bris.ac.uk, 3/1082394.grendel.chm.bris.ac.uk
     status = rectime=1560866239,varattr=,jobs=1082394.grendel.chm.bris.ac.uk,state=free,netload=6138834969431,gres=,loadave=3.99,ncpus=4,physmem=7387308kb,availmem=14275824kb,totmem=14774436kb,idletime=61529310,nusers=1,nsessions=1,sessions=8768,uname=Linux g-1-7.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-8
     state = job-exclusive
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1080748.grendel.chm.bris.ac.uk, 1/1080748.grendel.chm.bris.ac.uk, 2/1080748.grendel.chm.bris.ac.uk, 3/1080748.grendel.chm.bris.ac.uk
     status = rectime=1560866237,varattr=,jobs=1080748.grendel.chm.bris.ac.uk,state=free,netload=9933047750927,gres=,loadave=1.00,ncpus=4,physmem=7387308kb,availmem=13805684kb,totmem=14774436kb,idletime=61529312,nusers=1,nsessions=1,sessions=8026,uname=Linux g-1-8.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-9
     state = job-exclusive
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1080748.grendel.chm.bris.ac.uk, 1/1080748.grendel.chm.bris.ac.uk, 2/1080748.grendel.chm.bris.ac.uk, 3/1080748.grendel.chm.bris.ac.uk
     status = rectime=1560866261,varattr=,jobs=1080748.grendel.chm.bris.ac.uk,state=free,netload=11050605512069,gres=,loadave=0.00,ncpus=4,physmem=7387308kb,availmem=14566540kb,totmem=14774436kb,idletime=61529336,nusers=0,nsessions=0,uname=Linux g-1-9.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-10
     state = job-exclusive
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1080187.grendel.chm.bris.ac.uk, 1/1080187.grendel.chm.bris.ac.uk, 2/1080187.grendel.chm.bris.ac.uk, 3/1080187.grendel.chm.bris.ac.uk
     status = rectime=1560866242,varattr=,jobs=1080187.grendel.chm.bris.ac.uk,state=free,netload=7059494037525,gres=,loadave=4.00,ncpus=4,physmem=7387308kb,availmem=14029180kb,totmem=14774436kb,idletime=61529324,nusers=1,nsessions=1,sessions=26903,uname=Linux g-1-10.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-11
     state = job-exclusive
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1080757.grendel.chm.bris.ac.uk, 1/1080774.grendel.chm.bris.ac.uk, 2/1080772.grendel.chm.bris.ac.uk, 3/1081049.grendel.chm.bris.ac.uk
     status = rectime=1560866248,varattr=,jobs=33732.grendel.chm.bris.ac.uk 1081049.grendel.chm.bris.ac.uk 1080757.grendel.chm.bris.ac.uk 1080772.grendel.chm.bris.ac.uk 1080774.grendel.chm.bris.ac.uk,state=free,netload=7486869595006,gres=,loadave=3.05,ncpus=4,physmem=7387308kb,availmem=13204456kb,totmem=14774436kb,idletime=61529339,nusers=2,nsessions=6,sessions=2182 2812 9762 10204 14949 17537,uname=Linux g-1-11.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-12
     state = job-exclusive
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1082346.grendel.chm.bris.ac.uk, 1/1082346.grendel.chm.bris.ac.uk, 2/1082346.grendel.chm.bris.ac.uk, 3/1082346.grendel.chm.bris.ac.uk
     status = rectime=1560866262,varattr=,jobs=1082346.grendel.chm.bris.ac.uk,state=free,netload=8616333291854,gres=,loadave=3.98,ncpus=4,physmem=7387308kb,availmem=14285276kb,totmem=14773412kb,idletime=5889618,nusers=1,nsessions=1,sessions=15636,uname=Linux g-1-12.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-13
     state = job-exclusive
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1080206.grendel.chm.bris.ac.uk, 1/1080206.grendel.chm.bris.ac.uk, 2/1080206.grendel.chm.bris.ac.uk, 3/1080206.grendel.chm.bris.ac.uk
     status = rectime=1560866270,varattr=,jobs=1080206.grendel.chm.bris.ac.uk,state=free,netload=5202596064155,gres=,loadave=0.00,ncpus=4,physmem=7387308kb,availmem=14569296kb,totmem=14773412kb,idletime=61594109,nusers=0,nsessions=0,uname=Linux g-1-13.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-14
     state = free
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     status = rectime=1560866256,varattr=,jobs=,state=free,netload=7367991573277,gres=,loadave=0.00,ncpus=4,physmem=7387308kb,availmem=14558080kb,totmem=14773412kb,idletime=61594097,nusers=0,nsessions=0,uname=Linux g-1-14.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-15
     state = free
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     status = rectime=1560866239,varattr=,jobs=,state=free,netload=5208491735286,gres=,loadave=0.00,ncpus=4,physmem=7387308kb,availmem=14539604kb,totmem=14774436kb,idletime=61594083,nusers=0,nsessions=0,uname=Linux g-1-15.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-16
     state = free
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     status = rectime=1560866236,varattr=,jobs=,state=free,netload=7506591435045,gres=,loadave=0.00,ncpus=4,physmem=7387308kb,availmem=14609708kb,totmem=14773412kb,idletime=61594063,nusers=0,nsessions=0,uname=Linux g-1-16.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-17
     state = free
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     status = rectime=1560866256,varattr=,jobs=,state=free,netload=4469916787079,gres=,loadave=0.00,ncpus=4,physmem=7387308kb,availmem=14465052kb,totmem=14774436kb,idletime=61594104,nusers=0,nsessions=0,uname=Linux g-1-17.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-18
     state = job-exclusive
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1080638.grendel.chm.bris.ac.uk, 1/1080765.grendel.chm.bris.ac.uk, 2/1080639.grendel.chm.bris.ac.uk, 3/1080759.grendel.chm.bris.ac.uk
     status = rectime=1560866273,varattr=,jobs=1080638.grendel.chm.bris.ac.uk 1080639.grendel.chm.bris.ac.uk 1080765.grendel.chm.bris.ac.uk 1080759.grendel.chm.bris.ac.uk,state=free,netload=3152523975092,gres=,loadave=3.48,ncpus=4,physmem=7387308kb,availmem=12701688kb,totmem=14774436kb,idletime=61594123,nusers=2,nsessions=5,sessions=24443 32559 9255 9296 12032,uname=Linux g-1-18.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-19
     state = down
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx
     ntype = cluster
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-20
     state = job-exclusive
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1082397.grendel.chm.bris.ac.uk, 1/1080746.grendel.chm.bris.ac.uk, 2/1080770.grendel.chm.bris.ac.uk, 3/1080443.grendel.chm.bris.ac.uk
     status = rectime=1560866251,varattr=,jobs=1080443.grendel.chm.bris.ac.uk 1080746.grendel.chm.bris.ac.uk 1080770.grendel.chm.bris.ac.uk 1082397.grendel.chm.bris.ac.uk,state=free,netload=3484837535212,gres=,loadave=2.45,ncpus=4,physmem=7387308kb,availmem=13637368kb,totmem=14774436kb,idletime=22375978,nusers=2,nsessions=4,sessions=2227 8064 18494 21018,uname=Linux g-1-20.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-21
     state = free
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     status = rectime=1560866251,varattr=,jobs=,state=free,netload=6971931096042,gres=,loadave=0.00,ncpus=4,physmem=7387308kb,availmem=14557432kb,totmem=14774436kb,idletime=61594112,nusers=0,nsessions=0,uname=Linux g-1-21.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-22
     state = free
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     status = rectime=1560866260,varattr=,jobs=,state=free,netload=3479074590756,gres=,loadave=0.00,ncpus=4,physmem=7387308kb,availmem=14531068kb,totmem=14774436kb,idletime=61594121,nusers=0,nsessions=0,uname=Linux g-1-22.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-1-23
     state = free
     np = 4
     properties = group1,tmp200G,np4,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1080912.grendel.chm.bris.ac.uk
     status = rectime=1560866260,varattr=,jobs=1080912.grendel.chm.bris.ac.uk,state=free,netload=5113279814006,gres=,loadave=0.99,ncpus=4,physmem=7387308kb,availmem=13967552kb,totmem=14774436kb,idletime=37408941,nusers=1,nsessions=2,sessions=12231 12299,uname=Linux g-1-23.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-2-0
     state = job-exclusive
     np = 4
     properties = group2,np4,qchem
     ntype = cluster
     jobs = 0/1080550.grendel.chm.bris.ac.uk, 1/1080398.grendel.chm.bris.ac.uk, 2/1080279.grendel.chm.bris.ac.uk, 3/1080776.grendel.chm.bris.ac.uk
     status = rectime=1560866237,varattr=,jobs=1080550.grendel.chm.bris.ac.uk 1080279.grendel.chm.bris.ac.uk 1080398.grendel.chm.bris.ac.uk 1080776.grendel.chm.bris.ac.uk,state=free,netload=46961005476,gres=,loadave=4.20,ncpus=4,physmem=8192880kb,availmem=22321856kb,totmem=24577896kb,idletime=29364654,nusers=2,nsessions=5,sessions=1864 7214 17072 17669 31444,uname=Linux g-2-0.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 2

g-2-2
     state = job-exclusive
     np = 4
     properties = group2,np4,qchem
     ntype = cluster
     jobs = 0/1080270.grendel.chm.bris.ac.uk, 1/1080773.grendel.chm.bris.ac.uk, 2/1080275.grendel.chm.bris.ac.uk, 3/1080204.grendel.chm.bris.ac.uk
     status = rectime=1560866238,varattr=,jobs=1080204.grendel.chm.bris.ac.uk 1080270.grendel.chm.bris.ac.uk 1080275.grendel.chm.bris.ac.uk 1080773.grendel.chm.bris.ac.uk,state=free,netload=117878938228,gres=,loadave=4.02,ncpus=4,physmem=8189164kb,availmem=14623996kb,totmem=16378084kb,idletime=29364660,nusers=2,nsessions=5,sessions=2176 24567 30688 28861 30756,uname=Linux g-2-2.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-2-3
     state = down,job-exclusive
     np = 8
     properties = group2,np8
     ntype = cluster
     jobs = 0/1078247.grendel.chm.bris.ac.uk, 1/1078247.grendel.chm.bris.ac.uk, 2/1078247.grendel.chm.bris.ac.uk, 3/1078247.grendel.chm.bris.ac.uk, 4/1078248.grendel.chm.bris.ac.uk, 5/1078248.grendel.chm.bris.ac.uk, 6/1078248.grendel.chm.bris.ac.uk, 7/1078248.grendel.chm.bris.ac.uk
     status = rectime=1559317994,varattr=,jobs=1078247.grendel.chm.bris.ac.uk 1078248.grendel.chm.bris.ac.uk,state=free,netload=992598303,gres=,loadave=8.00,ncpus=8,physmem=16466260kb,availmem=31966620kb,totmem=32930124kb,idletime=2436196,nusers=1,nsessions=2,sessions=16760 16776,uname=Linux g-2-3.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-3-0
     state = down
     np = 8
     properties = group3,mem4G,tmp200G,np8,sse4_2,avx,qchem
     ntype = cluster
     status = rectime=1557738099,varattr=,jobs=,state=free,netload=10135937462828,gres=,loadave=0.09,ncpus=8,physmem=32960600kb,availmem=42587304kb,totmem=65919056kb,idletime=0,nusers=0,nsessions=0,uname=Linux g-3-0.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-3-1
     state = job-exclusive
     np = 8
     properties = group3,mem4G,tmp200G,np8,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1082363.grendel.chm.bris.ac.uk, 1/1082363.grendel.chm.bris.ac.uk, 2/1082364.grendel.chm.bris.ac.uk, 3/1082364.grendel.chm.bris.ac.uk, 4/1082365.grendel.chm.bris.ac.uk, 5/1082365.grendel.chm.bris.ac.uk, 6/1082365.grendel.chm.bris.ac.uk, 7/1082365.grendel.chm.bris.ac.uk
     status = rectime=1560866240,varattr=,jobs=1082363.grendel.chm.bris.ac.uk 1082364.grendel.chm.bris.ac.uk 1082365.grendel.chm.bris.ac.uk,state=free,netload=2368999662263,gres=,loadave=8.02,ncpus=8,physmem=32960600kb,availmem=63181100kb,totmem=65917008kb,idletime=29364698,nusers=3,nsessions=4,sessions=3856 23648 23693 23743,uname=Linux g-3-1.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-3-2
     state = job-exclusive
     np = 8
     properties = group3,mem4G,tmp200G,np8,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1080906.grendel.chm.bris.ac.uk, 1/1080906.grendel.chm.bris.ac.uk, 2/1082351.grendel.chm.bris.ac.uk, 3/1082351.grendel.chm.bris.ac.uk, 4/1080606.grendel.chm.bris.ac.uk, 5/1080606.grendel.chm.bris.ac.uk, 6/1080606.grendel.chm.bris.ac.uk, 7/1080606.grendel.chm.bris.ac.uk
     status = rectime=1560866260,varattr=,jobs=1080606.grendel.chm.bris.ac.uk 1080906.grendel.chm.bris.ac.uk 1082351.grendel.chm.bris.ac.uk,state=free,netload=15488654634731,gres=,loadave=8.00,ncpus=8,physmem=32960600kb,availmem=52159432kb,totmem=65917008kb,idletime=29364717,nusers=3,nsessions=4,sessions=4451 10998 14920 31307,uname=Linux g-3-2.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-3-4
     state = free
     np = 8
     properties = group3,mem4G,tmp200G,np8,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1082352.grendel.chm.bris.ac.uk, 1/1082352.grendel.chm.bris.ac.uk, 2/1082353.grendel.chm.bris.ac.uk, 3/1082353.grendel.chm.bris.ac.uk, 4/1080536.grendel.chm.bris.ac.uk
     status = rectime=1560866236,varattr=,jobs=1080536.grendel.chm.bris.ac.uk 1082352.grendel.chm.bris.ac.uk 1082353.grendel.chm.bris.ac.uk,state=free,netload=10409401338727,gres=,loadave=4.91,ncpus=8,physmem=32960612kb,availmem=64437340kb,totmem=65917020kb,idletime=17293812,nusers=3,nsessions=5,sessions=4856 4925 15054 19596 19646,uname=Linux g-3-4.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-3-5
     state = job-exclusive
     np = 8
     properties = group3,mem4G,tmp200G,np8,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1082354.grendel.chm.bris.ac.uk, 1/1082354.grendel.chm.bris.ac.uk, 2/1082356.grendel.chm.bris.ac.uk, 3/1082356.grendel.chm.bris.ac.uk, 4/1082318.grendel.chm.bris.ac.uk, 5/1082318.grendel.chm.bris.ac.uk, 6/1082357.grendel.chm.bris.ac.uk, 7/1082357.grendel.chm.bris.ac.uk
     status = rectime=1560866249,varattr=,jobs=1082318.grendel.chm.bris.ac.uk 1082354.grendel.chm.bris.ac.uk 1082356.grendel.chm.bris.ac.uk 1082357.grendel.chm.bris.ac.uk,state=free,netload=5441177875732,gres=,loadave=8.00,ncpus=8,physmem=32960600kb,availmem=63309608kb,totmem=65919056kb,idletime=29364700,nusers=1,nsessions=4,sessions=26998 29512 29560 29608,uname=Linux g-3-5.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-3-3
     state = job-exclusive
     np = 8
     properties = group3,mem4G,tmp200G,np8,sse4_2,avx,qchem
     ntype = cluster
     jobs = 0/1082358.grendel.chm.bris.ac.uk, 1/1082358.grendel.chm.bris.ac.uk, 2/1082359.grendel.chm.bris.ac.uk, 3/1082359.grendel.chm.bris.ac.uk, 4/1082362.grendel.chm.bris.ac.uk, 5/1082315.grendel.chm.bris.ac.uk, 6/1082315.grendel.chm.bris.ac.uk, 7/1082362.grendel.chm.bris.ac.uk
     status = rectime=1560866270,varattr=,jobs=1082315.grendel.chm.bris.ac.uk 1082358.grendel.chm.bris.ac.uk 1082359.grendel.chm.bris.ac.uk 1082362.grendel.chm.bris.ac.uk,state=free,netload=7430531897471,gres=,loadave=8.00,ncpus=8,physmem=32960600kb,availmem=63646896kb,totmem=65919056kb,idletime=29364725,nusers=2,nsessions=5,sessions=1879 15320 18502 18553 18722,uname=Linux g-3-3.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-2-1
     state = down
     np = 1
     ntype = cluster
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-2-20
     state = job-exclusive
     np = 16
     properties = group2,mem4G,tmp200G,np16,sse4_2,avx
     ntype = cluster
     jobs = 0/1082333.grendel.chm.bris.ac.uk, 1/1082333.grendel.chm.bris.ac.uk, 2/1082333.grendel.chm.bris.ac.uk, 3/1082333.grendel.chm.bris.ac.uk, 4/1082333.grendel.chm.bris.ac.uk, 5/1082333.grendel.chm.bris.ac.uk, 6/1082333.grendel.chm.bris.ac.uk, 7/1082333.grendel.chm.bris.ac.uk, 8/1082333.grendel.chm.bris.ac.uk, 9/1082333.grendel.chm.bris.ac.uk, 10/1082333.grendel.chm.bris.ac.uk, 11/1082333.grendel.chm.bris.ac.uk, 12/1082333.grendel.chm.bris.ac.uk, 13/1082333.grendel.chm.bris.ac.uk, 14/1082333.grendel.chm.bris.ac.uk, 15/1082333.grendel.chm.bris.ac.uk
     status = rectime=1560866240,varattr=,jobs=1082333.grendel.chm.bris.ac.uk,state=free,netload=24364058285132,gres=,loadave=12.72,ncpus=16,physmem=264633352kb,availmem=499774324kb,totmem=529263616kb,idletime=3035392,nusers=2,nsessions=4,sessions=8538 18951 26100 23392,uname=Linux g-2-20.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-0-2
     state = free
     np = 8
     ntype = cluster
     status = rectime=1560866258,varattr=,jobs=,state=free,netload=41929024097510,gres=,loadave=0.05,ncpus=8,physmem=33009308kb,availmem=65380200kb,totmem=66014868kb,idletime=29368407,nusers=0,nsessions=0,uname=Linux g-0-2.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-0-3
     state = free
     np = 8
     ntype = cluster
     status = rectime=1560866252,varattr=,jobs=1024558.grendel.chm.bris.ac.uk,state=free,netload=1686596087585,gres=,loadave=0.15,ncpus=8,physmem=33009308kb,availmem=65252140kb,totmem=66014868kb,idletime=1545564,nusers=1,nsessions=1,sessions=19075,uname=Linux g-0-3.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-0-4
     state = free
     np = 8
     ntype = cluster
     status = rectime=1560866250,varattr=,jobs=,state=free,netload=1497694239271,gres=,loadave=0.00,ncpus=8,physmem=33009308kb,availmem=65235244kb,totmem=66014868kb,idletime=29368400,nusers=0,nsessions=0,uname=Linux g-0-4.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

g-0-5
     state = free
     np = 8
     ntype = cluster
     status = rectime=1560866254,varattr=,jobs=,state=free,netload=1768840045537,gres=,loadave=0.00,ncpus=8,physmem=33009308kb,availmem=65355748kb,totmem=66014868kb,idletime=29368348,nusers=1,nsessions=1,sessions=23580,uname=Linux g-0-5.local 2.6.32-279.14.1.el6.x86_64 #1 SMP Tue Nov 6 23:43:09 UTC 2012 x86_64,opsys=linux
     mom_service_port = 15002
     mom_manager_port = 15003
     gpus = 0

