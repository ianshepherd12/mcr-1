#!/bin/bash



echo SCS-MP2

for i in *_charge
do

cd $i

file=rs/mp2.out
rs_e=$(grep 'EMP2' $file | sed -n 2~2p | awk '{print $3}')
file=ts/mp2.out
ts_e=$(grep 'EMP2' $file | sed -n 2~2p | awk '{print $3}')
file=ps/mp2.out
ps_e=$(grep 'EMP2' $file | sed -n 2~2p | awk '{print $3}')

echo "$i $rs_e $ts_e $ps_e"

cd ..

done



echo HF

for i in *_charge
do

cd $i

file=rs/mp2.out
rs_e=$(grep '!RHF STATE  1.1 Energy' $file | awk '{print $5}')
file=ts/mp2.out
ts_e=$(grep '!RHF STATE  1.1 Energy' $file | awk '{print $5}')
file=ps/mp2.out
ps_e=$(grep '!RHF STATE  1.1 Energy' $file | awk '{print $5}')

echo "$i $rs_e $ts_e $ps_e"

cd ..

done

