#!/bin/bash


list="ps ts rs"

cp ../+1_charge/1_submit.sh .


for i in $list
do


echo $i
mkdir $i

cp -r ../+1_charge/$i/lattice $i/
cp -r ../+1_charge/$i/mp2.com $i/

#sed -i 's/set,charge=-2/set,charge=0/' $i/mp2.com
sed -i 's/1+ point charge/1.5+ point charge/' $i/lattice
sed -i 's/    1.000000    /    1.500000    /'    $i/lattice


done
