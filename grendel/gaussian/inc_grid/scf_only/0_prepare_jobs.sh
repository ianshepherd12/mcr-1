#!/bin/bash



cp -r ../../scf_only/* .
rm -r hf
rm */*/*.o1*
rm */*/*.log
rm */*/*.chk

orig="GenECP SCF(qc,Maxcycle=1000) 5d 7f"
replacement="GenECP SCF(qc,Maxcycle=1000) Int=(Grid=Ultrafine) 5d 7f"


for i in */*/*.com
do

   echo ${i}
   sed -i "s@${orig}@${replacement}@" ${i}

done
