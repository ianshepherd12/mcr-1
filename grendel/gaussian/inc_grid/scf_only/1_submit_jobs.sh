#!/bin/bash

functionals="b3lyp bhandhlyp pbepbe pbe1pbe"
list="rs ts ps"


walltime="24:0:0"
node_no=1
proc_per_node=1



for i in ${functionals}
do
   cd ${i}

   for j in ${list}
   do
     cd ${j}
     #pwd
     #echo ${j}
     #echo ${j}_${i}
     qg09 ${j} -N ${j}_${i}_ultrafine -l walltime=${walltime},nodes=${node_no}:ppn=${proc_per_node}
     cd ..

   done

   #cd - > /dev/null
   cd ..

done

