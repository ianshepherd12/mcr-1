%nproc=1
%chk=ts
# hf/GenECP EmpiricalDispersion=GD3BJ SP SCF(qc,Maxcycle=1000) 5d 7f 

ts_hf

1 1
C          3.37329        0.21166        3.77124
C          3.78911       -1.23386        3.47460
C          2.64343       -2.12971        2.94818
C          2.16974       -1.69685        1.57237
O          2.76554       -2.11566        0.54828
O          1.17264       -0.88042        1.53965
C          2.18686       -1.08390       -3.95152
C          2.44945       -2.28682       -3.05306
C          3.90230       -2.39257       -2.60957
O          1.60729       -2.22160       -1.86257
P         -0.44251       -2.32818       -1.78952
O         -0.24270       -3.39033       -0.58882
O         -0.49903       -0.88909       -1.24554
O         -0.50202       -2.78456       -3.21086
C         -1.96332       -1.96145        5.37496
C         -1.80433       -0.86483        4.30624
C         -2.46001       -1.21801        3.00971
C         -1.93516       -1.63038        1.80746
N         -3.83457       -1.24889        2.82818
C         -4.09510       -1.66237        1.56350
N         -2.96097       -1.89946        0.91837
C          5.32787        2.95538       -0.61208
C          4.27967        2.18011       -1.40669
C          3.18934        1.55058       -0.54852
O          2.30819        0.85770       -1.20784
O          3.16845        1.70522        0.68549
C          2.43427        5.48060       -0.36376
C          0.92367        5.46049       -0.05072
C          0.48169        4.10181        0.38970
C          0.08871        2.99998       -0.32429
N          0.58348        3.65889        1.70111
C          0.29105        2.34409        1.74232
N         -0.01822        1.90345        0.52334
C         -5.44630        4.49854       -1.45555
C         -5.62917        3.02386       -1.85259
C         -4.48805        2.15694       -1.42984
C         -3.61642        1.38158       -2.14658
N         -4.10423        2.01061       -0.10067
C         -3.05409        1.18629       -0.01138
N         -2.74529        0.79632       -1.24613
Zn         0.82619        0.13218       -0.12403
H          4.21152        0.77229        4.20143
H          2.54536        0.23886        4.49229
H          3.05408        0.72770        2.86175
H          4.61100       -1.24274        2.74431
H          4.17769       -1.70118        4.39017
H          1.79808       -2.09279        3.64782
H          2.99923       -3.16508        2.88059
H          2.78138       -1.17852       -4.86902
H          2.46498       -0.15858       -3.43817
H          1.12975       -1.03647       -4.22906
H          2.14404       -3.20796       -3.56772
H          4.21063       -1.48746       -2.07175
H          4.54533       -2.50660       -3.49000
H          4.05531       -3.25858       -1.95510
H         -1.46252       -1.66694        6.30452
H         -1.52314       -2.90382        5.02866
H         -3.02091       -2.14398        5.60195
H         -0.73823       -0.69548        4.10453
H         -2.21292        0.08367        4.68626
H         -0.89720       -1.73328        1.53001
H         -2.64150       -2.22147       -0.53149
H         -5.09549       -1.77145        1.15965
H          6.07871        3.38518       -1.28612
H          5.84300        2.30202        0.10089
H          4.87029        3.77153       -0.04433
H          3.78337        2.83058       -2.14267
H          4.74546        1.37337       -1.99218
H          2.75152        6.48360       -0.67321
H          2.66728        4.77750       -1.17053
H          3.01988        5.18947        0.51569
H          0.35667        5.74375       -0.94714
H          0.68903        6.20263        0.72535
H         -0.07914        2.90560       -1.38930
H          0.34292        1.73555        2.63754
H         -6.30393        5.08900       -1.79602
H         -4.53829        4.91517       -1.90661
H         -5.37018        4.61392       -0.36722
H         -6.56118        2.63520       -1.41482
H         -5.72992        2.93457       -2.94160
H         -3.54684        1.19764       -3.21036
H         -1.97060        0.14231       -1.47162
H         -2.53723        0.89706        0.89367
O         -2.37726       -2.44429       -1.53435
C         -3.01960       -3.68016       -1.91430
H         -2.82401       -3.84139       -2.97471
H         -2.62308       -4.51240       -1.32231
H         -4.09339       -3.56873       -1.73765
C          0.40511       -4.66210       -0.82246
H          0.27747       -5.22733        0.10305
H         -0.07315       -5.18710       -1.65524
H          1.46591       -4.50566       -1.03151
H          2.09841       -2.08017       -0.99672
H          0.88903        4.23538        2.51137
H         -4.54695       -1.02494        3.55101
H         -4.56001        2.47822        0.71408

Zn 0
SDD
****
C H N 0
6-31G(d)
****
O P 0
6-31+G(d,p)
****

Zn 0
SDD

