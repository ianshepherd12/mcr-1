%nproc=1
%chk=ps
# pbepbe/GenECP EmpiricalDispersion=GD3BJ SP SCF(qc,Maxcycle=1000) 5d 7f 

ps_pbepbe

1 1
C          3.84793        2.06671        2.52534
C          4.84945        0.92801        2.31320
C          4.21750       -0.48829        2.37684
C          3.26114       -0.69816        1.24412
O          3.82305       -1.07822        0.11253
O          2.03918       -0.49492        1.36023
C          1.63706       -2.06687       -3.85838
C          1.97150       -3.22532       -2.92675
C          3.45857       -3.53411       -2.87022
O          1.58340       -2.90612       -1.54681
P          0.04111       -2.91614       -1.04636
O          0.20537       -3.50858        0.45478
O         -0.38150       -1.44386       -0.78641
O         -0.87016       -3.71755       -1.92758
C          0.04396       -0.75544        6.17755
C          0.42958       -0.33450        4.74634
C         -0.75199       -0.10495        3.85612
C         -1.18449       -0.73057        2.70705
N         -1.71046        0.86399        4.10965
C         -2.65788        0.79142        3.13734
N         -2.36977       -0.16410        2.26626
C          3.79125        3.55216       -2.79940
C          2.91892        2.31166       -2.96848
C          2.60197        1.59632       -1.66336
O          1.85466        0.53563       -1.78494
O          3.01857        2.02385       -0.57447
C          0.31657        5.14523       -2.35254
C         -0.42209        5.22088       -1.00727
C         -0.36586        3.90286       -0.30283
C         -0.26325        2.63551       -0.81157
N         -0.31094        3.74125        1.07272
C         -0.16141        2.42910        1.35752
N         -0.13146        1.72737        0.22822
C         -6.77904        1.79615       -0.71114
C         -5.59176        1.98150       -1.67217
C         -4.62358        0.84184       -1.66204
C         -3.32457        0.76614       -1.23547
N         -4.92997       -0.42672       -2.14058
C         -3.87056       -1.23367       -2.00906
N         -2.88354       -0.52136       -1.47226
Zn         0.79567       -0.00411       -0.18932
H          4.36762        3.03164        2.52837
H          3.33334        1.95888        3.48930
H          3.10189        2.09215        1.72931
H          5.35809        1.04942        1.34734
H          5.62547        0.95985        3.08951
H          3.66563       -0.61133        3.31647
H          5.00834       -1.24618        2.31774
H          1.95127       -2.31637       -4.87925
H          2.14518       -1.15363       -3.53642
H          0.55734       -1.87497       -3.87044
H          1.40798       -4.12256       -3.21979
H          4.02362       -2.65066       -2.54665
H          3.81561       -3.82624       -3.86474
H          3.65972       -4.35455       -2.17214
H          0.94229       -0.92301        6.78289
H         -0.54290       -1.68135        6.16510
H         -0.55574        0.01866        6.67209
H          1.05058       -1.11124        4.28151
H          1.04931        0.57537        4.78766
H         -0.71715       -1.55134        2.17369
H         -3.71086       -1.09378        1.37694
H         -3.52925        1.43678        3.10981
H          3.92721        4.06053       -3.76140
H          4.78123        3.28751       -2.41075
H          3.34299        4.26086       -2.09607
H          1.95516        2.57004       -3.43260
H          3.38823        1.58474       -3.64816
H          0.26329        6.10850       -2.87234
H         -0.12767        4.38725       -3.00781
H          1.36852        4.89260       -2.19994
H         -1.46698        5.52902       -1.16626
H          0.03587        5.98985       -0.36914
H         -0.22741        2.31355       -1.84228
H         -0.06466        2.02319        2.35634
H         -7.44528        2.66451       -0.76331
H         -6.43014        1.68657        0.32232
H         -7.36374        0.90424       -0.96741
H         -5.96419        2.12703       -2.69720
H         -5.03410        2.88902       -1.40708
H         -2.68671        1.51383       -0.78935
H         -1.94864       -0.90319       -1.22807
H         -3.82030       -2.28167       -2.28236
O         -4.36895       -1.75044        1.03524
C         -3.70354       -3.00954        0.96416
H         -4.42972       -3.74850        0.61072
H         -2.85800       -2.98403        0.26141
H         -3.32954       -3.32715        1.94836
C          0.59815       -4.88783        0.61333
H          0.62170       -5.07704        1.68875
H         -0.13075       -5.54931        0.13280
H          1.59355       -5.04969        0.18399
H          3.13791       -1.11256       -0.63089
H         -0.34666        4.50955        1.77363
H         -1.71764        1.51373        4.91891
H         -5.85471       -0.72197       -2.52627

Zn 0
SDD
****
C H N 0
6-31G(d)
****
O P 0
6-31+G(d,p)
****

Zn 0
SDD

