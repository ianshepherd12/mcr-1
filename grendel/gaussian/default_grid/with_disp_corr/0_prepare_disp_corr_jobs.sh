#!/bin/bash

functionals="b3lyp bhandhlyp pbepbe pbe1pbe"
list="rs ts ps"

for i in ${functionals}
do

   mkdir ${i}
   cd ${i}

   for j in ${list}
   do
     mkdir ${j}
     cd ${j}
     cp ../../../${i}/${j}/${j}.com ${j}.com

     sed -i 's/GenECP SCF(qc,Maxcycle=1000) 5d 7f/GenECP EmpiricalDispersion=GD3BJ SP SCF(qc,Maxcycle=1000) 5d 7f/' ${j}.com

     cd ..

   done

   cd ..

done
