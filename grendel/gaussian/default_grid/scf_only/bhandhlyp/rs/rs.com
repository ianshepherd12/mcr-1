%nproc=1
%chk=rs
# bhandhlyp/GenECP SCF(qc,Maxcycle=1000) 5d 7f 

rs_bhandhlyp

1 1
C          3.13838       -0.21826        4.02461
C          3.00200       -1.74614        3.97155
C          1.75373       -2.24867        3.21511
C          1.82720       -2.04221        1.70338
O          2.68887       -2.66456        1.04262
O          0.96411       -1.22696        1.19472
C          2.42012       -1.05003       -3.81856
C          2.91429       -2.05558       -2.78254
C          4.37889       -1.85240       -2.40951
O          2.07224       -1.97140       -1.59777
P         -1.10135       -1.96745       -2.16898
O         -0.91229       -3.08179       -1.00603
O         -0.60055       -0.61335       -1.62362
O         -0.70362       -2.45030       -3.52679
C         -2.39104       -2.19911        5.15388
C         -2.10573       -1.00987        4.21992
C         -2.72447       -1.18093        2.87161
C         -2.16688       -1.40194        1.63941
N         -4.10036       -1.18822        2.67172
C         -4.37432       -1.39953        1.37910
N         -3.21157       -1.53195        0.74169
C          5.51316        2.64687       -0.06355
C          4.54498        2.14200       -1.12567
C          3.47101        1.21490       -0.57659
O          2.49258        0.96391       -1.38189
O          3.57032        0.72557        0.56773
C          2.73113        5.29878        0.13379
C          1.23289        5.15506        0.46704
C          0.84480        3.72611        0.67543
C          0.39293        2.76464       -0.19248
N          1.04359        3.05413        1.87469
C          0.73697        1.74911        1.69304
N          0.33823        1.53307        0.44405
C         -5.09457        4.77407       -1.52311
C         -5.44148        3.27546       -1.51864
C         -4.29300        2.44721       -1.04845
C         -3.46556        1.57232       -1.70082
N         -3.79321        2.53317        0.24598
C         -2.70933        1.75749        0.37206
N         -2.49997        1.16650       -0.80115
Zn         1.14657       -0.27942       -0.58142
H          4.01580        0.07360        4.61295
H          2.25488        0.23746        4.49222
H          3.24992        0.20589        3.02067
H          3.89638       -2.18360        3.50741
H          2.95425       -2.14138        4.99601
H          0.85512       -1.75476        3.60577
H          1.64887       -3.32904        3.38900
H          2.97424       -1.19736       -4.75361
H          2.57899       -0.02811       -3.46610
H          1.35584       -1.20491       -4.01604
H          2.77747       -3.07478       -3.18053
H          4.53989       -0.84325       -2.01575
H          5.01338       -1.98764       -3.29422
H          4.69696       -2.57738       -1.64926
H         -1.91589       -2.03657        6.12763
H         -1.99670       -3.12851        4.72725
H         -3.46764       -2.32805        5.31941
H         -1.02425       -0.89455        4.07764
H         -2.47364       -0.07864        4.67578
H         -1.12808       -1.46224        1.34030
H         -3.11162       -1.67228       -0.27622
H         -5.36036       -1.45370        0.93150
H          6.26347        3.31213       -0.50847
H          6.03633        1.81506        0.41946
H          4.98554        3.20559        0.71738
H          4.05068        2.97293       -1.64611
H          5.08359        1.57826       -1.90422
H          2.99904        6.35410        0.00250
H          2.97218        4.76206       -0.78966
H          3.35283        4.88444        0.93579
H          0.63226        5.56469       -0.35576
H          0.99074        5.74486        1.36301
H          0.14621        2.86776       -1.24113
H          0.84154        0.98853        2.45393
H         -5.94812        5.36093       -1.88055
H         -4.23941        4.97212       -2.17935
H         -4.84062        5.12712       -0.51610
H         -6.31771        3.09445       -0.87864
H         -5.70861        2.94425       -2.52995
H         -3.47757        1.21216       -2.72071
H         -1.72319        0.50616       -1.02033
H         -2.10911        1.62930        1.26247
O         -2.73986       -1.74604       -2.06004
C         -3.58703       -2.73366       -2.69115
H         -3.36802       -2.78375       -3.76124
H         -3.43330       -3.71643       -2.23015
H         -4.61589       -2.40198       -2.53154
C         -0.07557       -4.24837       -1.15934
H         -0.66428       -5.11064       -0.83312
H          0.22726       -4.37176       -2.20171
H          0.80560       -4.12718       -0.52588
H          2.50006       -2.43883       -0.83390
H          1.42044        3.47125        2.74801
H         -4.82050       -1.07061        3.42034
H         -4.18430        3.13565        1.00369

Zn 0
SDD
****
C H N 0
6-31G(d)
****
O P 0
6-31+G(d,p)
****

Zn 0
SDD

