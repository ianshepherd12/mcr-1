#!/bin/bash

functionals="b3lyp bhandhlyp pbepbe pbe1pbe"
list="rs ts ps"


walltime="24:0:0"
node_no=1
proc_per_node=1

cd with_disp_corr

for i in ${functionals}
do
   cd ${i}
   echo ${i}

   for j in ${list}
   do
     cd ${j}
     #pwd     
     #echo ${j}
     #echo ${j}_${i}
     grep 'SCF Done' ${j}.log | awk '{print $5}'
     cd ..

   done

   #cd - > /dev/null
   cd ..

done
