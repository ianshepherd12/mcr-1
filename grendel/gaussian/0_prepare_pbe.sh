#!/bin/bash

functionals="pbepbe"
list="rs ts ps"


walltime="24:0:0"
node_no=1
proc_per_node=1



for i in ${functionals}
do

   mkdir ${i}
   cd ${i}

   for j in ${list}
   do
     mkdir ${j}
     cd ${j}
     cp ../../hf/${j}/${j}.com ${j}.com
     sed -i "s@hf@${i}@" ${j}.com

     cd ..

   done

   cd ..

done
