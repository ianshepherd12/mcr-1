#!/bin/bash

out_file=dft.com

for i in ../../geom_files/*.xyz
do
   file=${i##*/}
   folder=${file%.xyz}
   
   echo $file $folder

   mkdir ${folder}
   
   cat header > ${folder}/${out_file}
   cat ${i} >> ${folder}/${out_file}
   cat footer >> ${folder}/${out_file}
   

done
