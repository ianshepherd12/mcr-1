#!/bin/bash

queue=hmem
mem=490G
node_no=1
proc_per_node=28
walltime="0:10:0"
molpro_script="6_threads_no_helper"

sbatch --job-name=$1 -o OUT -p ${queue} --mem=${mem} --nodes=${node_no} --tasks-per-node=${proc_per_node} --time=${walltime} --mail-type=NONE ${molpro_script} $1

