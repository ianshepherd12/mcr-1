#!/bin/bash --noprofile

walltime="24:0:0"
node_no=1
proc_per_node=2
job_name="loop_test"


./qscript dft.com -N ${job_name} -l walltime=${walltime},nodes=${node_no}:ppn=${proc_per_node}
