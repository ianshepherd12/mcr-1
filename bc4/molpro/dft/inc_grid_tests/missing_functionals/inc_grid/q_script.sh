#!/bin/bash

queue=serial
mem=3G
node_no=1
proc_per_node=1
walltime="48:0:0"

molpro_script="${HOME}/projects/mcr-1/molpro/dft/inc_grid_tests/default_grid/simons_molpro_t1_n1_h_off"

sbatch --job-name=$1 -o OUT -p ${queue} --mem=${mem} --nodes=${node_no} --tasks-per-node=${proc_per_node} --time=${walltime} --mail-type=NONE ${molpro_script} $1
