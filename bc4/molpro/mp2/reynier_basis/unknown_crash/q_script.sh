#!/bin/bash

queue=serial
mem=32G
node_no=1
proc_per_node=9
walltime="48:0:0"

molpro_script="${HOME}/scripts/molpro/global_install/molpro_2019.1/n2_no_single_t1"

sbatch --job-name=$1 -o OUT -p ${queue} --mem=${mem} --nodes=${node_no} --tasks-per-node=${proc_per_node} --time=${walltime} --mail-type=NONE ${molpro_script} $1
