#!/bin/bash

out_file=energy_data.dat

#stationary_points
list="rs ts ps"

rm ${out_file}

for rc_val in $list; do

    echo ${rc_val}
    grep -A 9 ' ELEC         E' ${rc_val}/*.out >> ${out_file}

done



