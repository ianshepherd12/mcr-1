#!/bin/bash

#stationary_points
string_list="B3_ PBE_"
string_list_2="186.0 196.0 206.0 216.0 226.0 236.0"
#string_list_2="138.0 148.0 158.0 168.0 178.0 188\.0"

data_file=energy_data.dat



for string in $string_list; do
    printf "${string}  "
    grep ${string} ${data_file} | awk '{print $3}' | tr "\n" " "
    echo ""
done

for string in $string_list_2; do
    printf "${string}  "
    grep ${string} ${data_file} | awk '{print $2}' | tr "\n" " "
    echo ""
done

echo ""
