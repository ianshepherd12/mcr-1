#!/bin/bash

out_file=b3lyp-in-pbe.com

for i in ~/projects/mcr-1/geom_files/*.xyz

do
   file=${i##*/}
   folder=${file%.xyz}
   
   echo $folder

   mkdir ${folder}
   
   cat header > ${folder}/${out_file}
   cat ${i} >> ${folder}/${out_file}
   cat footer >> ${folder}/${out_file}
   

done
