#!/bin/bash

input_file="*.out"
out_file=data.dat
temp_file=data_temp.dat

rc_list=(rs ts ps)
trunc="no_trunc"


get_no_elec () {
grep 'Total number of active electrons' $1 | awk '{print $6}' 2> /dev/null
}

get_mp2_energy () {
grep 'EMP2_SCS         =' $1 | awk '{print $3}'
}

get_hf_energy () {
grep '!RHF STATE  1.1 Energy' $1 | awk '{print $5}'
}



#redirect standard error because the above function writes out to stderr if input file not present 
#could alternatively add a if [-f file]; then check to each function instead - would need to set function output to null too
exec 3>&2
exec 2> /dev/null

rm ${out_file}
rm -r ${out_folder}
mkdir ${out_folder}

rm ${elec_file}
rm ${dft_embed_energy_file}

echo "location No_elec SCS-MP2-in-B3LYP_energy" > ${temp_file}


for i in region_*
do

  region=$i
  echo $region

  for rc in ${rc_list[@]}
  do
    file=${region}/${trunc}/${rc}/${input_file}
    no_elec=$(get_no_elec ${file})
    no_elec=${no_elec%.00}
    dft_in_dft=$(get_mp2_energy ${file})
    hf_in_dft=$(get_hf_energy ${file})
    echo ${file} ${no_elec} ${dft_in_dft} ${hf_in_dft}>> ${temp_file}

  done

done

#Reinstate stderr
exec 2>&3


column -t ${temp_file} > ${out_file}
rm ${temp_file}
