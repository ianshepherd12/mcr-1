#!/bin/bash

list="region_3/no_trunc/ts/ region_4/no_trunc/rs/ region_4/no_trunc/ts/ region_4/no_trunc/ps/"

move_dir=disk_full_error


for dir in ${list}
do

   cd $dir
   #rc_val=${dir%*/}
   #rc_val=${rc_val##*rc}
   mkdir ${move_dir}
   mv OUT ${move_dir}  
   mv *.out ${move_dir}  
   echo $dir

   ../q_script.sh *.com
   cd - > /dev/null

done

