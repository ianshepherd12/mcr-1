#!/bin/bash

for i in reg*/no_trunc
do
  echo $i
  cd $i
  sed -i 's/500,mw/400,mw/' header
  rm -r ./?s/
  ./0_setup_script.sh
  ./1_submit.sh
  cd ../..

done
